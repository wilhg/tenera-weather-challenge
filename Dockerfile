FROM azul/zulu-openjdk-alpine:11-jre

ENV APP weather-challenge-0.0.1-SNAPSHOT
ENV PORT 8080

ADD ./build/libs/$APP.jar $APP.jar
ADD ./build/resources/main build/resources/main

EXPOSE $PORT
ENTRYPOINT java -jar $APP.jar
