### How to run

Please make sure you have Java 11 installed on your machine, if it's Java 8, please change the corresponding code in the `build.gradle.kts`.

Go to the project fold and run

```bash
./gradlew bootRun
```

When the application started, please visit

```
http://localhost:8080/swagger-ui.html
```

At here you will see the endpoints APIs, and knowing how to query it.

### How does a production ready micro service should look like

- With health check
- With circuit break
- With good logging
- With request tracing (e.g. Zipkin, Jaeger)
- With monitoring (e.g. Newrelic, Datalog)

### How to deploy the application to AWS

I suppose the ECS infrastructures are ready on AWS, what I need is only to deploy the application itself. And this application is the first application that using Terraform. Then I will:

1. Choose an existing Terraform module form Terraform Registry. For example https://github.com/cloudposse/terraform-aws-ecs-web-app
2. Create a `.tf` file which import this module, fulfil with proper parameters.
3. The CI/CD will read this `.tf` file and apply it, then the application will be deployed to AWS ECS.