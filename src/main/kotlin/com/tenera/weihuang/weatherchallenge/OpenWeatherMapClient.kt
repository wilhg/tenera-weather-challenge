package com.tenera.weihuang.weatherchallenge

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Component
class OpenWeatherMapClient {
    @Value("\${external.open_weather_map.api_key}")
    private lateinit var apiKey: String

    @Value("\${external.open_weather_map.url.current_weather}")
    private lateinit var currentWeatherUrl: String

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private val client = HttpClient.newHttpClient()

    companion object {
        private val UMBRELLA_WEATHERS = setOf("Thunderstorm", "Drizzle", "Rain")
    }

    fun getCurrentWeather(location: String): String {
        val request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("${currentWeatherUrl}?q=$location&appid=$apiKey&units=metric"))
                .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println("Status: ${response.statusCode()}")
        println("Body: ${response.body()}")

        if (response.statusCode() != HttpStatus.OK.value()) {
            throw RuntimeException("Something wrong when query Open weather map: \n${response.body()}")
        }
        return response.body()
    }

    fun parseCurrentWeather(responseBody: String): CityWeather {
        val weather = objectMapper.readValue(responseBody, WeatherDTO::class.java)

        return CityWeather(
                id = weather.id,
                name = weather.name,
                temperature = weather.main.temp,
                airPressure = weather.main.pressure,
                umbrellaNeeded = weather.weathers.getOrNull(0)
                        ?.let { UMBRELLA_WEATHERS.contains(it.main) }
                        ?: false
        )
    }
}