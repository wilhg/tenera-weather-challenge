package com.tenera.weihuang.weatherchallenge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WeatherChallengeApplication

fun main(args: Array<String>) {
	runApplication<WeatherChallengeApplication>(*args)
}
