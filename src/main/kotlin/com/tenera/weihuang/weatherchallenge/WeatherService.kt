package com.tenera.weihuang.weatherchallenge

import com.google.common.collect.EvictingQueue
import com.google.common.collect.Queues
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Service
class WeatherService {
    @Autowired
    private lateinit var client: OpenWeatherMapClient

    private val nameToCityId = ConcurrentHashMap<String, Int>()
    private val cityIdToWeathers = ConcurrentHashMap<Int, Queue<CityWeather>>()

    @CircuitBreaker(name = "currentWeather")
    fun getCurrentWeather(location: String): CityWeather {
        val weather = client.getCurrentWeather(location)
                .let { client.parseCurrentWeather(it) }

        nameToCityId.computeIfAbsent(location) { weather.id }

        cityIdToWeathers.computeIfAbsent(weather.id) { newWeathers() }
        cityIdToWeathers[weather.id]!!.offer(weather)

        return weather
    }

    fun getHistoryWeather(location: String): List<CityWeather> {
        return nameToCityId[location]
                .let { id -> cityIdToWeathers.getOrDefault(id, newWeathers()) }
                .toList()
    }

    private fun newWeathers(): Queue<CityWeather> {
        return Queues.synchronizedQueue(EvictingQueue.create<CityWeather>(5))
    }
}