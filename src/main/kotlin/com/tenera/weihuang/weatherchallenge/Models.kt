package com.tenera.weihuang.weatherchallenge

import com.fasterxml.jackson.annotation.JsonProperty

data class CityWeather(
        val id: Int,
        val name: String,
        val temperature: Float,
        val airPressure: Int,
        val umbrellaNeeded: Boolean
)

data class WeatherDTO(
        val id: Int,
        val name: String,
        val main: WeatherMainDTO,

        @JsonProperty("weather")
        val weathers: List<WeatherWeatherDTO>
)

data class WeatherMainDTO(
        val temp: Float,
        val pressure: Int
)

data class WeatherWeatherDTO(
        val main: String
)