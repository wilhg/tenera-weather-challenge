package com.tenera.weihuang.weatherchallenge

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.validation.constraints.Pattern

@Controller
@RequestMapping("/weather")
class WeatherController {

    @Autowired
    private lateinit var weatherService: WeatherService

    @GetMapping("/{location}/current")
    @ResponseBody
    fun getCurrentWeather(
            @Pattern(regexp = "[\\w,]+", message = "The location contains illegal character.")
            @PathVariable("location") location: String): CityWeather {
        return weatherService.getCurrentWeather(location)
    }

    @GetMapping("/{location}/history")
    @ResponseBody
    fun getHistoryWeather(
            @Pattern(regexp = "[\\w,]+", message = "The location contains illegal character.")
            @PathVariable("location") location: String): List<CityWeather> {
        return weatherService.getHistoryWeather(location)
    }
}