package com.tenera.weihuang.weatherchallenge

import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.fasterxml.jackson.core.type.TypeReference
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.impl.annotations.SpyK
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

class WeatherIntegrationTest : IntegrationTest() {

    @BeforeEach
    fun setup() {
        clearAllMocks()
    }

    @SpyK
    @Autowired
    private lateinit var client: OpenWeatherMapClient

    @ParameterizedTest
    @CsvSource(value = ["Thunderstorm, true", "Drizzle, true", "Rain, true", "Cloudy, false"])
    fun `should return correct umbrellaNeeded`(weather: String, umbrellaNeeded: Boolean) {
        givenCurrentWeatherResponse(weather, 1, "Berlin")

        val respEntity = restTemplate.getForEntity("${getHost()}/weather/Berlin/current", String::class.java)

        verify(exactly = 1) { client.getCurrentWeather("Berlin") }
        assertThat(respEntity.statusCode).isEqualTo(HttpStatus.OK)

        val cityWeather = objectMapper.readValue(respEntity.body, CityWeather::class.java)

        assertThat(cityWeather.umbrellaNeeded).isEqualTo(umbrellaNeeded)
        assertThat(cityWeather.temperature).isEqualTo(19.18f)
        assertThat(cityWeather.airPressure).isEqualTo(1018)
        assertThat(cityWeather.name).isEqualTo("Berlin")
        assertThat(cityWeather.id).isEqualTo(1)
    }

    @Test
    fun `should return correct history weathers`() {
        givenCurrentWeatherResponse(20.1f, 1, "Berlin", "Berlin")
        givenCurrentWeatherResponse(20.2f, 1, "Berlin", "berlin")
        givenCurrentWeatherResponse(20.3f, 1, "Berlin", "Berlin,de")
        givenCurrentWeatherResponse(20.4f, 1, "Berlin", "berlin,de")

        givenCurrentWeatherResponse(21.1f, 2, "London", "London")
        givenCurrentWeatherResponse(21.2f, 2, "London", "london")
        givenCurrentWeatherResponse(21.3f, 2, "London", "London,gb")
        givenCurrentWeatherResponse(21.4f, 2, "London", "london,gb")

        restTemplate.getForEntity("${getHost()}/weather/Berlin/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/berlin/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/Berlin,de/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/berlin,de/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/Berlin/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/berlin/current", String::class.java)

        restTemplate.getForEntity("${getHost()}/weather/London/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/london/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/London,gb/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/london,gb/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/London/current", String::class.java)
        restTemplate.getForEntity("${getHost()}/weather/london/current", String::class.java)

        verify(exactly = 12) { client.getCurrentWeather(any()) }

        val berlin1Response = restTemplate.getForEntity("${getHost()}/weather/Berlin/history", String::class.java)
        assertThat(berlin1Response.statusCode).isEqualTo(HttpStatus.OK)
        val berlin1Weather = objectMapper.readValue(berlin1Response.body, object : TypeReference<List<CityWeather>>() {})
        println(berlin1Weather)
        assertThat(berlin1Weather).hasSize(5)
        assertThat(berlin1Weather.map { it.temperature }).containsExactly(21.2f, 21.3f, 21.4f, 21.1f, 21.2f)

        val berlin2Response = restTemplate.getForEntity("${getHost()}/weather/berlin,de/history", String::class.java)
        assertThat(berlin2Response.statusCode).isEqualTo(HttpStatus.OK)
        val berlin2Weather = objectMapper.readValue(berlin1Response.body, object : TypeReference<List<CityWeather>>() {})
        assertThat(berlin2Weather).isEqualTo(berlin2Weather)

    }


    fun givenCurrentWeatherResponse(weather: String, id: Int, city: String) {
        every { client.getCurrentWeather(city) } returns """
            {"coord":{"lon":13.41,"lat":52.52},"weather":[{"id":800,"main":"$weather","gbscription":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":19.18,"feels_like":17.75,"temp_min":15,"temp_max":22.22,"pressure":1018,"humidity":60},"visibility":10000,"wind":{"speed":2.6,"deg":60},"clouds":{"all":1},"dt":1596230897,"sys":{"type":1,"id":1275,"country":"DE","sunrise":1596165896,"sunset":1596222022},"timezone":7200,"id":$id,"name":"$city","cod":200}
        """.trimIndent()
    }

    fun givenCurrentWeatherResponse(temp: Float, id: Int, city: String, location: String) {
        every { client.getCurrentWeather(location) } returns """
            {"coord":{"lon":13.41,"lat":52.52},"weather":[{"id":800,"main":"Cloudy","gbscription":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":$temp,"feels_like":17.75,"temp_min":15,"temp_max":22.22,"pressure":1018,"humidity":60},"visibility":10000,"wind":{"speed":2.6,"deg":60},"clouds":{"all":1},"dt":1596230897,"sys":{"type":1,"id":1275,"country":"DE","sunrise":1596165896,"sunset":1596222022},"timezone":7200,"id":$id,"name":"$city","cod":200}
        """.trimIndent()
    }
}