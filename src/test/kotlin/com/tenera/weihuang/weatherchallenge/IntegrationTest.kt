package com.tenera.weihuang.weatherchallenge

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.spyk
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import


@Import(TestConfig::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class IntegrationTest {

    @LocalServerPort
    lateinit var port: Integer

    @Autowired
    internal lateinit var restTemplate: TestRestTemplate

    @Autowired
    internal lateinit var objectMapper: ObjectMapper

    fun getHost(): String {
        return "http://localhost:$port"
    }
}

@TestConfiguration
class TestConfig {
    @Bean
    fun client(): OpenWeatherMapClient {
        return spyk(OpenWeatherMapClient(), recordPrivateCalls = true)
    }
}